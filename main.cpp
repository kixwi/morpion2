#include <iostream>

int main() {
  char stop;
  //printing the pattern/layout
  std::cout << " |A|B|C|" << '\n';
  std::cout << "_|_|_|_|" << "\n";
  std::cout << "1| | | |" << "\n";

  //to stoping the prog.
  std::cout << "Stop ?" << std::endl;
  std::cin >> stop;

  return 0;
}
